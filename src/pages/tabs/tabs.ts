import { Component } from '@angular/core';

import { TablePage } from '../table/table';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { KanaPage } from '../kana/kana';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TablePage;
  tab3Root = ContactPage;
  tab4Root = KanaPage;

  constructor() {

  }
}
