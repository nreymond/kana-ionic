import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-table',
  templateUrl: 'table.html'
})
export class TablePage {

  constructor() {
    this.initializeItems();

  }

  hiraganas;
  katakanas;



    initializeItems() {
      this.hiraganas = [{ "hiragana": "あ", "katakana": "ア", "roumaji": "a"},
      { "hiragana": "い", "katakana": "イ", "roumaji": "i"},
      { "hiragana": "う", "katakana": "ウ", "roumaji": "u"},
      { "hiragana": "え", "katakana": "エ", "roumaji": "e"},
      { "hiragana": "お", "katakana": "オ", "roumaji": "o"},
      { "hiragana": "か", "katakana": "カ", "roumaji": "ka"},
      { "hiragana": "き", "katakana": "キ", "roumaji": "ki"},
      { "hiragana": "く", "katakana": "ク", "roumaji": "ku"},
      { "hiragana": "け", "katakana": "ケ", "roumaji": "ke"},
      { "hiragana": "こ", "katakana": "コ", "roumaji": "ko"},
      { "hiragana": "さ", "katakana": "サ", "roumaji": "sa"},
      { "hiragana": "し", "katakana": "シ", "roumaji": "shi"},
      { "hiragana": "す", "katakana": "ス", "roumaji": "su"},
      { "hiragana": "せ", "katakana": "セ", "roumaji": "se"},
      { "hiragana": "そ", "katakana": "ソ", "roumaji": "so"},
      { "hiragana": "た", "katakana": "タ", "roumaji": "ta"},
      { "hiragana": "ち", "katakana": "チ", "roumaji": "chi"},
      { "hiragana": "つ", "katakana": "ツ", "roumaji": "tsu"},
      { "hiragana": "て", "katakana": "テ", "roumaji": "te"},
      { "hiragana": "と", "katakana": "ト", "roumaji": "to"},
      { "hiragana": "な", "katakana": "ナ", "roumaji": "na"},
      { "hiragana": "に", "katakana": "ニ", "roumaji": "ni"},
      { "hiragana": "ぬ", "katakana": "ヌ", "roumaji": "nu"},
      { "hiragana": "ね", "katakana": "ネ", "roumaji": "ne"},
      { "hiragana": "の", "katakana": "ノ", "roumaji": "no"},
      { "hiragana": "は", "katakana": "ハ", "roumaji": "ha"},
      { "hiragana": "ひ", "katakana": "ヒ", "roumaji": "hi"},
      { "hiragana": "ふ", "katakana": "フ", "roumaji": "fu"},
      { "hiragana": "へ", "katakana": "ヘ", "roumaji": "he"},
      { "hiragana": "ほ", "katakana": "ホ", "roumaji": "ho"},
      { "hiragana": "ま", "katakana": "マ", "roumaji": "ma"},
      { "hiragana": "み", "katakana": "ミ", "roumaji": "mi"},
      { "hiragana": "む", "katakana": "ム", "roumaji": "mu"},
      { "hiragana": "め", "katakana": "メ", "roumaji": "me"},
      { "hiragana": "も", "katakana": "モ", "roumaji": "mo"},
      { "hiragana": "や", "katakana": "ヤ", "roumaji": "ya"},
      { "hiragana": "ゆ", "katakana": "ユ", "roumaji": "yu"},
      { "hiragana": "よ", "katakana": "ヨ", "roumaji": "yo"},
      { "hiragana": "ら", "katakana": "ラ", "roumaji": "ra"},
      { "hiragana": "り", "katakana": "リ", "roumaji": "ri"},
      { "hiragana": "る", "katakana": "ル", "roumaji": "ru"},
      { "hiragana": "れ", "katakana": "レ", "roumaji": "re"},
      { "hiragana": "ろ", "katakana": "ロ", "roumaji": "ro"},
      { "hiragana": "わ", "katakana": "ワ", "roumaji": "wa"},
      { "hiragana": "を", "katakana": "ヲ", "roumaji": "wo"},
      { "hiragana": "ん", "katakana": "ン", "roumaji": "n"}];

    }
  }
