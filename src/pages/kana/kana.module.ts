import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KanaPage } from './kana';

@NgModule({
  declarations: [
    KanaPage,
  ],
  imports: [
    IonicPageModule.forChild(KanaPage),
  ],
})
export class KanaPageModule {}
