import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
// import hiragana from './hiragana.ts'
/**
 * Generated class for the KanaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kana',
  templateUrl: 'kana.html',
})
export class KanaPage {

  public selections: any = {
    first: 'hiragana',
    second: 'katakana',
    selectedSelection: ''
  };

  constructor() {
    this.initializeItems();
    this.kana = "hiragana";
  }

  public kana = "hiragana";



  hiraganas;
  katakanas;

  initializeItems() {
    this.hiraganas = [{ "kana": "あ", "roumaji": "a" },
    { "kana": "い", "roumaji": "i" },
    { "kana": "う", "roumaji": "u" },
    { "kana": "え", "roumaji": "e" },
    { "kana": "お", "roumaji": "o" },
    { "kana": "か", "roumaji": "ka" },
    { "kana": "き", "roumaji": "ki" },
    { "kana": "く", "roumaji": "ku" },
    { "kana": "け", "roumaji": "ke" },
    { "kana": "こ", "roumaji": "ko" },
    { "kana": "さ", "roumaji": "sa" },
    { "kana": "し", "roumaji": "shi" },
    { "kana": "す", "roumaji": "su" },
    { "kana": "せ", "roumaji": "se" },
    { "kana": "そ", "roumaji": "so" },
    { "kana": "た", "roumaji": "ta" },
    { "kana": "ち", "roumaji": "chi" },
    { "kana": "つ", "roumaji": "tsu" },
    { "kana": "て", "roumaji": "te" },
    { "kana": "と", "roumaji": "to" },
    { "kana": "な", "roumaji": "na" },
    { "kana": "に", "roumaji": "ni" },
    { "kana": "ぬ", "roumaji": "nu" },
    { "kana": "ね", "roumaji": "ne" },
    { "kana": "の", "roumaji": "no" },
    { "kana": "は", "roumaji": "ha" },
    { "kana": "ひ", "roumaji": "hi" },
    { "kana": "ふ", "roumaji": "fu" },
    { "kana": "へ", "roumaji": "he" },
    { "kana": "ほ", "roumaji": "ho" },
    { "kana": "ま", "roumaji": "ma" },
    { "kana": "み", "roumaji": "mi" },
    { "kana": "む", "roumaji": "mu" },
    { "kana": "め", "roumaji": "me" },
    { "kana": "も", "roumaji": "mo" },
    { "kana": "や", "roumaji": "ya" },
    { "kana": "ゆ", "roumaji": "yu" },
    { "kana": "よ", "roumaji": "yo" },
    { "kana": "ら", "roumaji": "ra" },
    { "kana": "り", "roumaji": "ri" },
    { "kana": "る", "roumaji": "ru" },
    { "kana": "れ", "roumaji": "re" },
    { "kana": "ろ", "roumaji": "ro" },
    { "kana": "わ", "roumaji": "wa" },
    { "kana": "を", "roumaji": "wo" },
    { "kana": "ん", "roumaji": "n" }];

    this.katakanas = [{ "kana": "ア", "roumaji": "a" },
    { "kana": "イ", "roumaji": "i" },
    { "kana": "ウ", "roumaji": "u" },
    { "kana": "エ", "roumaji": "e" },
    { "kana": "オ", "roumaji": "o" },
    { "kana": "カ", "roumaji": "ka" },
    { "kana": "キ", "roumaji": "ki" },
    { "kana": "ク", "roumaji": "ku" },
    { "kana": "ケ", "roumaji": "ke" },
    { "kana": "コ", "roumaji": "ko" },
    { "kana": "サ", "roumaji": "sa" },
    { "kana": "シ", "roumaji": "shi" },
    { "kana": "ス", "roumaji": "su" },
    { "kana": "セ", "roumaji": "se" },
    { "kana": "ソ", "roumaji": "so" },
    { "kana": "タ", "roumaji": "ta" },
    { "kana": "チ", "roumaji": "chi" },
    { "kana": "ツ", "roumaji": "tsu" },
    { "kana": "テ", "roumaji": "te" },
    { "kana": "ト", "roumaji": "to" },
    { "kana": "ナ", "roumaji": "na" },
    { "kana": "ニ", "roumaji": "ni" },
    { "kana": "ヌ", "roumaji": "nu" },
    { "kana": "ネ", "roumaji": "ne" },
    { "kana": "ノ", "roumaji": "no" },
    { "kana": "ハ", "roumaji": "ha" },
    { "kana": "ヒ", "roumaji": "hi" },
    { "kana": "フ", "roumaji": "hu" },
    { "kana": "ヘ", "roumaji": "he" },
    { "kana": "ホ", "roumaji": "ho" },
    { "kana": "マ", "roumaji": "ma" },
    { "kana": "ミ", "roumaji": "mi" },
    { "kana": "ム", "roumaji": "mu" },
    { "kana": "メ", "roumaji": "me" },
    { "kana": "モ", "roumaji": "mo" },
    { "kana": "ヤ", "roumaji": "ya" },
    { "kana": "ユ", "roumaji": "yu" },
    { "kana": "ヨ", "roumaji": "yo" },
    { "kana": "ラ", "roumaji": "ra" },
    { "kana": "リ", "roumaji": "ri" },
    { "kana": "ル", "roumaji": "ru" },
    { "kana": "レ", "roumaji": "re" },
    { "kana": "ロ", "roumaji": "ro" },
    { "kana": "ワ", "roumaji": "wa" },
    { "kana": "ヲ", "roumaji": "wo" },
    { "kana": "ン", "roumaji": "n" }];

  }


}
